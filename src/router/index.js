import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/About.vue"),
    },
    {
        path: "/terms",
        name: "Terms and Conditions",
        component: () => import("../views/Terms.vue"),
    },
    {
        path: "/cart",
        name: "Cart",
        component: () => import("../views/Cart.vue"),
    },
    {
        path: "/checkout",
        name: "checkout",
        component: () => import("../views/Checkout.vue"),
    },
    {
        path: "/product/:id",
        name: "Product Detail View",
        component: () => import("../components/ProductDetailView.vue"),
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
