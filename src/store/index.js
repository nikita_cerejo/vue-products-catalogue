import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        count: 0,
        cart_products: [],
        products: [],
    },
    mutations: {
        products(state, products) {
            state.products = products;
        },
        updateCart(state, data) {
            var product = data.product;
            var type = data.type;
            var prod_quantity = data.quantity; // prod_quantity -1 = remove all

            if ("add" == type) {
                if (!(product.id in state.cart_products)) {
                    state.cart_products[product.id] = {
                        id: product.id,
                        name: product.name,
                        price: product.price,
                        image: product.image,
                        quantity: 0,
                    };
                }
                state.cart_products[product.id].quantity++;
            }

            if ("remove" == type && state.cart_products[product.id]) {
                if (
                    state.cart_products[product.id].quantity > 1 &&
                    -1 != prod_quantity
                ) {
                    state.cart_products[product.id].quantity--;
                } else {
                    state.cart_products = state.cart_products.filter(function(
                        prodt
                    ) {
                        return prodt.id != product.id;
                    });
                }
            }
            state.count = state.cart_products.filter((n) => n).length;
        },
        count(state) {
            state.count = state.cart_products.filter((n) => n).length;
        },
    },
    actions: {},
    modules: {},
});
